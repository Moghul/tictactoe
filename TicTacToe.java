import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

public class TicTacToe extends JFrame
{
	private static final long serialVersionUID = 1L;

	// ServerSocket, Socket, Input and Output Streams
	private ServerSocket serverSocket = null;
	private Socket connection = null;
	private ObjectInputStream instream = null;
	private ObjectOutputStream outstream = null;

	private Dimension screenSize; // screen size
	private int width; // width of screen
	private int height; // height of screen

	private JButton b1, b2, b3, b4, b5, b6, b7, b8, b9; // buttons XO fields
	/*
	 * Button position: [b1][b2][b3] [b4][b5][b6] [b7][b8][b9]
	 */

	private JTextArea textArea; // text area on right side of frame for chat and
								// notifications
	private JScrollPane sp; // scroll pane for text area

	private JTextField ip, port, nick, message; // IP address, port number,
												// nickname, chat message
	private JButton join, create, newGame; // buttons : JOIN, CREATE, NEW GAME

	private String logic[] = { "", "", "", "", "", "", "", "", "" }; // FIELDS
																		// XO
	/*
	 * Explanation: for ex. if logic = { "X", "X", "X", ...}; then X win
	 * [X][X][X] [ ][ ][ ] [ ][ ][ ] 2nd ex. if logic = { "O","","", "","O","",
	 * "","","O" }; 0 win [O][ ][ ] [ ][O][ ] [ ][ ][O]
	 */

	private String xo = ""; // server is X , client is O
	private String server, client, msg; // nick1 server, nick2 client, chat
										// message

	private boolean signal; // signal for "WHOSE TURN"
	private boolean signalfoo = true; // if this signal is false, then stop
										// sending messages over connection

	private int roundNR = 0; // The number of parties // 0=X play first; 1=O
								// play 2nd; 3=X turn....
	private int occupiedSlots = 0; // The number of moves, if occupiedSlots>=9 ,
									// game is a draw

	// --- Constructor ---
	public TicTacToe()
	{
		initUI();
	}

	// --- User Interface ---
	private void initUI()
	{
		screenSize = Toolkit.getDefaultToolkit().getScreenSize(); // get screen
																	// size
		width = (int) screenSize.getWidth(); // get width
		height = (int) screenSize.getHeight(); // get height

		setSize(width / 2, height / 2); // set the size of a frame to 1/3 and
										// 1/2 of a window screen
		setResizable(true);
		setTitle("TicTacToe");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);

		setLayout(new BorderLayout());

		// This is the main panel for x/o buttons
		JPanel xoPanel = new JPanel();
		xoPanel.setLayout(new GridLayout(3, 3));

		b1 = new JButton("...");
		b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b1.setText(xo); // set X or O button text
					sendMessage(xo + "1");
					sendMessage("true");
					signal = false;
					b1.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[0] = "X";
					} else
					{
						logic[0] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b1);

		b2 = new JButton("...");
		b2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b2.setText(xo);
					sendMessage(xo + "2");
					sendMessage("true");
					signal = false;
					b2.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[1] = "X";
					} else
					{
						logic[1] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b2);

		b3 = new JButton("...");
		b3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b3.setText(xo);
					sendMessage(xo + "3");
					sendMessage("true");
					signal = false;
					b3.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[2] = "X";
					} else
					{
						logic[2] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b3);

		b4 = new JButton("...");
		b4.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b4.setText(xo);
					sendMessage(xo + "4");
					sendMessage("true");
					signal = false;
					b4.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[3] = "X";
					} else
					{
						logic[3] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b4);

		b5 = new JButton("...");
		b5.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b5.setText(xo);
					sendMessage(xo + "5");
					sendMessage("true");
					signal = false;
					b5.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[4] = "X";
					} else
					{
						logic[4] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b5);

		b6 = new JButton("...");
		b6.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b6.setText(xo);
					sendMessage(xo + "6");
					sendMessage("true");
					signal = false;
					b6.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[5] = "X";
					} else
					{
						logic[5] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b6);

		b7 = new JButton("...");
		b7.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b7.setText(xo);
					sendMessage(xo + "7");
					sendMessage("true");
					signal = false;
					b7.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[6] = "X";
					}

					else
					{
						logic[6] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b7);

		b8 = new JButton("...");
		b8.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b8.setText(xo);
					sendMessage(xo + "8");
					sendMessage("true");
					signal = false;
					b8.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[7] = "X";
					} else
					{
						logic[7] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b8);

		b9 = new JButton("...");
		b9.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (signal)
				{
					b9.setText(xo);
					sendMessage(xo + "9");
					sendMessage("true");
					signal = false;
					b9.setEnabled(false);
					if (xo.equals("X"))
					{
						logic[8] = "X";
					} else
					{
						logic[8] = "O";
					}
					++occupiedSlots;
					checkTable();
				}
			}
		});
		xoPanel.add(b9);

		enableButtons(false); // set all buttons on false while we wait for
								// client to join
		add(xoPanel, BorderLayout.CENTER);

		// other components (east panel)
		JPanel pEast = new JPanel();
		pEast.setLayout(new BorderLayout());
		pEast.setPreferredSize(new Dimension(270, height));
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setEditable(false);
		sp = new JScrollPane(textArea);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pEast.add(sp, BorderLayout.CENTER);
		add(pEast, BorderLayout.EAST);

		
		message = new JTextField(" ");
		message.setEditable(false);

		ip = new JTextField("127.0.0.1");
		ip.setToolTipText("Enter Host IP address");
		ip.setPreferredSize(new Dimension(100, 25));
		port = new JTextField("9999");
		port.setToolTipText("Enter Host PORT nubmer, default:9999");
		port.setPreferredSize(new Dimension(100, 25));
		nick = new JTextField();
		nick.setToolTipText("Enter your Nickname");
		nick.setPreferredSize(new Dimension(100, 25));

		// create match btn
		create = new JButton("Create");
		create.setToolTipText("Host game");
		create.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				if (nick.getText().equals("") || nick.getText().equals(" "))
				{
					try
					{
						JOptionPane.showMessageDialog(null,
								"You need a nickname!");
					} catch (ExceptionInInitializerError e)
					{
					}
					return;
				}

				new CreateButtonThread("CreateButton"); // so that the frame
														// doesn't freeze, we
														// use a thread
			}
		});

		// the join btn
		join = new JButton("Join");
		join.setToolTipText("Join game");
		join.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				try
				{
					if (nick.getText().equals("") || nick.getText().equals(" "))
					{
						try
						{
							JOptionPane.showMessageDialog(null,
									"You need a nickname");
						} catch (ExceptionInInitializerError e)
						{
						}
						return;
					}

					connection = new Socket(ip.getText(), Integer.parseInt(port
							.getText()));

					outstream = new ObjectOutputStream(connection
							.getOutputStream());
					outstream.flush();
					instream = new ObjectInputStream(connection
							.getInputStream());

					msg = (String) instream.readObject();
					textArea.append(msg + "\n");
					scrollToBottom();

					xo = "O";
					signal = false;

					client = nick.getText();

					msg = (String) instream.readObject(); // get nick from host
					server = "" + msg;

					sendMessage(client);

					enableButtons(true);
					message.setEditable(true);

					ip.setEnabled(false);
					port.setEnabled(false);
					nick.setEnabled(false);

					textArea.append("X is first\n");
					scrollToBottom();

					join.setEnabled(false);
					create.setEnabled(false);
					ip.setEnabled(false);
					port.setEnabled(false);
					nick.setEnabled(false);

					new ReceiveMessages("Message from server"); // thread to
																// receive data
																// from host
				} catch (Exception e)
				{
					closeAll();
					runAll();
					try
					{
						JOptionPane.showMessageDialog(null,
								"Server is offline \n" + e);
					} catch (ExceptionInInitializerError ex)
					{
					}
				}
			}
		});

		// new game button
		newGame = new JButton("New Game");
		newGame.setToolTipText("Start new game");
		newGame.setEnabled(false);
		newGame.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				sendMessage("Start new game"); // send request to client, for
												// new game

				++roundNR;

				for (int i = 0; i < logic.length; i++)
				{
					logic[i] = "";
				}

				if (roundNR % 2 == 0)
				{
					signal = true;
					textArea.append("X  plays first!\n");
					scrollToBottom();
					sendMessage("false");
					sendMessage("X  plays first!");
				} else
				{
					signal = false;
					sendMessage("true");
					textArea.append("O plays first!\n");
					scrollToBottom();
					sendMessage("O  plays first!");
				}

				setBText();
				enableButtons(true);
				newGame.setEnabled(false);
			}
		});

		JPanel pNorth = new JPanel();
		pNorth.add(ip);
		pNorth.add(port);
		pNorth.add(nick);
		pNorth.add(create);
		pNorth.add(join);
		pNorth.add(newGame);
		add(pNorth, BorderLayout.NORTH);

		// this takes the default IP from the computer (localhost)
		addWindowListener(new WindowAdapter()
		{
			public void windowActivated(WindowEvent event)
			{
				try
				{
					InetAddress thisIp = InetAddress.getLocalHost();
					ip.setText(thisIp.getHostAddress());
				} catch (Exception e)
				{
					ip.setText("127.0.0.1");
				}
			}

			public void windowClosing(WindowEvent event)
			{
				if (connection != null)
				{
					sendMessage("Going offline");
				}
				closeAll();
			}
		});
	}

	// thread to create buttons...
	private class CreateButtonThread implements Runnable
	{
		public CreateButtonThread(String name)
		{
			new Thread(this, name).start();
		}

		public void run()
		{
			try
			{
				join.setEnabled(false);
				create.setEnabled(false);
				port.setEnabled(false);
				nick.setEnabled(false);

				serverSocket = new ServerSocket(
						Integer.parseInt(port.getText()));

				textArea.append("Waiting for client...\n");
				scrollToBottom();
				connection = serverSocket.accept();

				outstream = new ObjectOutputStream(connection.getOutputStream());
				outstream.flush();
				instream = new ObjectInputStream(connection.getInputStream());
				sendMessage(nick.getText() + ": Successfully connected!");
				textArea.append("Client Successfully connected!\n");
				scrollToBottom();

				xo = "X";
				signal = true;

				server = nick.getText();

				sendMessage(server);

				msg = (String) instream.readObject(); // first nick to server
				client = "" + msg;

				enableButtons(true);
				message.setEditable(true);
				ip.setEnabled(false);

				textArea.append("X plays first!\n");
				scrollToBottom();
				new ReceiveMessages("Message from client");
			} catch (Exception e)
			{
				closeAll();
				runAll();
				try
				{
					JOptionPane.showMessageDialog(null,
							"Could not create game\n" + e);
				} catch (ExceptionInInitializerError exc)
				{
				}
			}
		}
	}

	// this is where i check if someone won or not. i know it looks ugly but i
	// think it's a good solution
	private void checkTable()
	{
		// for x
		if (
		// check x vertically
		(logic[0].equals("X") && logic[1].equals("X") && logic[2].equals("X"))
				|| (logic[3].equals("X") && logic[4].equals("X") && logic[5]
						.equals("X"))
				|| (logic[6].equals("X") && logic[7].equals("X") && logic[8]
						.equals("X"))
				||
				// check x horizontally
				(logic[0].equals("X") && logic[3].equals("X") && logic[6]
						.equals("X"))
				|| (logic[1].equals("X") && logic[4].equals("X") && logic[7]
						.equals("X"))
				|| (logic[2].equals("X") && logic[5].equals("X") && logic[8]
						.equals("X"))
				||
				// check x diagonally
				(logic[0].equals("X") && logic[4].equals("X") && logic[8]
						.equals("X"))
				|| (logic[2].equals("X") && logic[4].equals("X") && logic[6]
						.equals("X")))
		{
			occupiedSlots = 0;
			enableButtons(false);
			JOptionPane.showMessageDialog(null, server + " X WINS ");
			if (xo.equals("X"))
			{
				newGame.setEnabled(true);
			}
		}
		// for 0

		else if (
		// 0 horizontally
		(logic[0].equals("O") && logic[1].equals("O") && logic[2].equals("O"))
				|| (logic[3].equals("O") && logic[4].equals("O") && logic[5]
						.equals("O"))
				|| (logic[6].equals("O") && logic[7].equals("O") && logic[8]
						.equals("O"))
				||
				// 0 vertically
				(logic[0].equals("O") && logic[3].equals("O") && logic[6]
						.equals("O"))
				|| (logic[1].equals("O") && logic[4].equals("O") && logic[7]
						.equals("O"))
				|| (logic[2].equals("O") && logic[5].equals("O") && logic[8]
						.equals("O"))
				||
				// 0 diagonally
				(logic[0].equals("O") && logic[4].equals("O") && logic[8]
						.equals("O"))
				|| (logic[2].equals("O") && logic[4].equals("O") && logic[6]
						.equals("O")))
		{
			occupiedSlots = 0;
			enableButtons(false);
			JOptionPane.showMessageDialog(null, client + " 0 WINS ");
			if (xo.equals("X"))
			{
				newGame.setEnabled(true);
			}
		} else
		{
			// this checks if it's a draw.
			if (occupiedSlots >= 9)
			{
				occupiedSlots = 0;
				sendMessage("IT'S A TIE");
				JOptionPane.showMessageDialog(null, "IT'S A TIE");
				if (xo.equals("X"))
				{
					newGame.setEnabled(true);
				}
			}
		}
	}

	// enable/disable buttons for turns
	private void enableButtons(boolean b)
	{
		b1.setEnabled(b);
		b2.setEnabled(b);
		b3.setEnabled(b);
		b4.setEnabled(b);
		b5.setEnabled(b);
		b6.setEnabled(b);
		b7.setEnabled(b);
		b8.setEnabled(b);
		b9.setEnabled(b);
	}

	// sets buttons to default
	private void setBText()
	{
		b1.setText("...");
		b2.setText("...");
		b3.setText("...");
		b4.setText("...");
		b5.setText("...");
		b6.setText("...");
		b7.setText("...");
		b8.setText("...");
		b9.setText("...");
	}

	// sending data over connection
	private void sendMessage(String p)
	{
		try
		{
			if (signalfoo)
			{
				outstream.writeObject(p);
				outstream.flush();
			}
		} catch (SocketException e)
		{
			if (signalfoo)
			{
				signalfoo = false;
				closeAll();
				runAll();
			}
		} catch (Exception e)
		{
			if (signalfoo)
			{
				signalfoo = false;
				closeAll();
				runAll();
				try
				{
					JOptionPane.showMessageDialog(null,"Sending data/Disconnect:\n" + e);
				} catch (ExceptionInInitializerError exc)
				{
				}
			}
		}
	}

	// receiving data/messages
	private class ReceiveMessages implements Runnable
	{
		private boolean siggy;
		private String name;

		public ReceiveMessages(String i)
		{
			siggy = true;
			name = i;
			new Thread(this, name).start();
		}

		public void run()
		{
			while (siggy)
			{
				try
				{
					msg = "";
					msg = (String) instream.readObject(); // receive messages

					if (name.equals("Message from server")) // client receive
															// data from
															// host/server
					{
						if (msg.equalsIgnoreCase("true"))
						{
							signal = true;
						} else if (msg.equalsIgnoreCase("false"))
						{
							signal = false;
						} else if (msg.equalsIgnoreCase("IT'S A TIE"))
						{
							JOptionPane.showMessageDialog(null, "IT'S A TIE");
						} else if (msg.equalsIgnoreCase("Start new game"))
						{
							for (int i = 0; i < logic.length; i++)
							{
								logic[i] = "";
							}
							signal = true;
							setBText();
							enableButtons(true);
						} else if (msg.equalsIgnoreCase("X1"))
						{
							b1.setText("X");
							logic[0] = "X";
							b1.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X2"))
						{
							b2.setText("X");
							logic[1] = "X";
							b2.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X3"))
						{
							b3.setText("X");
							logic[2] = "X";
							b3.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X4"))
						{
							b4.setText("X");
							logic[3] = "X";
							b4.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X5"))
						{
							b5.setText("X");
							logic[4] = "X";
							b5.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X6"))
						{
							b6.setText("X");
							logic[5] = "X";
							b6.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X7"))
						{
							b7.setText("X");
							logic[6] = "X";
							b7.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X8"))
						{
							b8.setText("X");
							logic[7] = "X";
							b8.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("X9"))
						{
							b9.setText("X");
							logic[8] = "X";
							b9.setEnabled(false);
							checkTable();
						}
					} else if (name.equals("Message from client")) // host/server
																	// receive
																	// data from
																	// client
					{
						if (msg.equalsIgnoreCase("true"))
						{
							signal = true;
						} else if (msg.equalsIgnoreCase("O1"))
						{
							++occupiedSlots;
							b1.setText("O");
							logic[0] = "O";
							b1.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O2"))
						{
							++occupiedSlots;
							b2.setText("O");
							logic[1] = "O";
							b2.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O3"))
						{
							++occupiedSlots;
							b3.setText("O");
							logic[2] = "O";
							b3.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O4"))
						{
							++occupiedSlots;
							b4.setText("O");
							logic[3] = "O";
							b4.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O5"))
						{
							++occupiedSlots;
							b5.setText("O");
							logic[4] = "O";
							b5.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O6"))
						{
							++occupiedSlots;
							b6.setText("O");
							logic[5] = "O";
							b6.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O7"))
						{
							++occupiedSlots;
							b7.setText("O");
							logic[6] = "O";
							b7.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O8"))
						{
							++occupiedSlots;
							b8.setText("O");
							logic[7] = "O";
							b8.setEnabled(false);
							checkTable();
						} else if (msg.equalsIgnoreCase("O9"))
						{
							++occupiedSlots;
							b9.setText("O");
							logic[8] = "O";
							b9.setEnabled(false);
							checkTable();
						}
					}
				} catch (Exception e)
				{
					siggy = false;
					closeAll();
					runAll();
					try
					{
						JOptionPane.showMessageDialog(null,
								"Receiving Data Failed/Disconnect:\n" + e);
					} catch (ExceptionInInitializerError exc)
					{
					}
				}
			}
		}
	}

	// reset game
	private void runAll()
	{
		setBText();
		enableButtons(false);

		msg = "";
		xo = "";
		signalfoo = true;
		occupiedSlots = 0;
		roundNR = 0;

		for (int i = 0; i < logic.length; i++)
		{
			logic[i] = "";
		}

		ip.setEnabled(true);
		port.setEnabled(true);
		nick.setEnabled(true);
		create.setEnabled(true);
		join.setEnabled(true);

		newGame.setEnabled(false);
		message.setEditable(false);
	}

	// turns off streams
	private void closeAll()
	{
		try
		{
			outstream.flush();
		} catch (Exception e)
		{
		}
		try
		{
			outstream.close();
		} catch (Exception e)
		{
		}
		try
		{
			instream.close();
		} catch (Exception e)
		{
		}
		try
		{
			serverSocket.close();
		} catch (Exception e)
		{
		}
		try
		{
			connection.close();
		} catch (Exception e)
		{
		}
	}

	// when you receive a message, it automatically scrolls to it
	public void scrollToBottom()
	{
		textArea.setCaretPosition(textArea.getText().length());
	}

	// main method
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				new TicTacToe().setVisible(true);
			}
		});
	}
}